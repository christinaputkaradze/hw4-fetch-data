
function sendRequest(link) {
    return new Promise((resolve, reject) => {
        const request = new XMLHttpRequest();
        request.open("GET", link);
        request.onload = () => resolve(JSON.parse(request.response));
        request.send();
    })
}

async function getData() {
    const filmsLink = "https://ajax.test-danit.com/api/swapi/films";
    let result = null;

    await sendRequest(filmsLink)
        .then(films => {
            console.log(films);
            return films;
        })
        .then(async films => {
            let fullFilmsInfo = await Promise.all(films.map(async film => {
                let characters = await Promise.all(film.characters.map(async characterLink => {
                    let characterName;

                    await sendRequest(characterLink)
                        .then(content => {
                            let name = content.name;
                            //console.log(name);

                            characterName = content.name;
                        });
                    return characterName;

                }));
                //console.log(characters);

                const { episodeId, name, openingCrawl } = film;
                console.log(({ episodeId, name, openingCrawl, characters }));
                return ({ episodeId, name, openingCrawl, characters });
            }));

            return fullFilmsInfo;
        })
        .then(films => {
            films.sort((film1, film2) => film1.episodeId - film2.episodeId);
            return films;
        })
        .then(data => result = data)


    return result;
}

function showFilms(films) {
    let main = document.querySelector('.main');

    for (const film of films) {
        let filmItem = document.createElement('div');
        filmItem.classList.add('film_item');
        main.append(filmItem);

        let episode = document.createElement('p');
        episode.classList.add('episode');
        episode.innerHTML = `<span> Episode: </span> ${film.episodeId}`;
        filmItem.append(episode);

        let name = document.createElement('p');
        name.classList.add('name');
        name.innerHTML = `<span> Name: </span> ${film.name}`;
        filmItem.append(name);

        let openingCrawl = document.createElement('p');
        openingCrawl.classList.add('crawl')
        openingCrawl.innerHTML = `<span> Opening crawl: </span> ${film.openingCrawl}`;
        filmItem.append(openingCrawl);

        let characters = document.createElement('ul');
        characters.classList.add('characters_list')
        characters.innerHTML = `<span> Characters: </span>`
        filmItem.append(characters);

        for (const character of film.characters) {
            let characterName = document.createElement('li');
            characterName.innerText = `${character}`
            characters.append(characterName);
        }
    }
}

let spinner = document.querySelector('.spinner');
spinner.style.display = 'flex';

getData()
    .then(data => { console.log(data); return data;})
    .then(data => {spinner.style.display = "none"; return data;})
    .then(data => showFilms(data))


